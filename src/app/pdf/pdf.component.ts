import { Component, OnInit } from '@angular/core';
import PSPDFKit from 'pspdfkit';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    PSPDFKit.load( {
      container: '#pspdfkit',
      document: '/assets/API Testing Document.pdf',
      baseUrl: location.protocol + '//' + location.host + '/assets/',
      licenseKey: 'QQXEWUmqA4mhmXA8BBb11HOchbN4udUwD3uY_pw0lUX26MWzBAg7kF01Vx6ErZqi-R0lAlsJyTU6MadJKYEvpwwuHwnbsRcuPxlZmVLlkL79tZ40IzYHpQZja93yoUgLIiMEkTYDccGhMjhDXJ8ehFl--IkIl-3pb2nojxRr-ow2a4x8sKgjMCB-KExu-NjQr7Mp-mvGj0myfFdDIjsuX5AmHTr9vYLlVUqHRNhKgppI1z0sSYT10BRrdIIZ7K_a0uhiIfp0nBlHpUML4qYQPOmtz74cvFeOL5qveJZaKENsidMCy5VLwmWrcxzEwHgOjaQUOZ23CLV3jo0-wVYgbqbw0IvbJx8G4-t0_qvalcK8Km8IAY9W9IkjiF8_0ldJS6h-08Wp-lgQOLG5kS66rcPzYdTEs2n-BWGAn_2HVyjAZomexvY6I45tR-xo5v5W'
    }).then(instance => {
      console.log('PSPDFKit loaded', instance);
    })
      .catch(error => {
        console.error(error.message);
      });
  }

}
