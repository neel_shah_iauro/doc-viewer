import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-doc',
  templateUrl: './doc.component.html',
  styleUrls: ['./doc.component.css']
})
export class DocComponent implements OnInit {
  docURL: any;

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.docURL = this.sanitizer.bypassSecurityTrustResourceUrl('https://docs.google.com/gview?url=https://filesamples.com/samples/document/docx/sample1.docx&embedded=true');
  }

}
