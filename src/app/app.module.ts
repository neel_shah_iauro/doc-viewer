import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { PdfComponent } from './pdf/pdf.component';
import { DocComponent } from './doc/doc.component';
const routes: Routes = [
  { path: '', redirectTo: '/pdf', pathMatch: 'full' },
  {
    path: 'pdf',
    component: PdfComponent
  }, {
    path: 'doc',
    component: DocComponent
  }
];
@NgModule({
  declarations: [
    AppComponent,
    PdfComponent,
    DocComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
